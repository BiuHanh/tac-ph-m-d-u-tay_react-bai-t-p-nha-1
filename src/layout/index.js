import React from "react";

import Navbar from "./navbar";
import Carousel from "./carousel";
import Smartphone from "./smartphone";
import Laptop from "./laptop";
import Promotion from "./promotion";

export default function Layout() {
  return (
    <>
      <Navbar />
      <Carousel />
      <Smartphone />
      <Laptop />
      <Promotion />
    </>
  );
}
